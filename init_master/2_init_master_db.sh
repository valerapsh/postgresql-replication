#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "postgres" <<-EOSQL
    CREATE ROLE replicant WITH REPLICATION LOGIN PASSWORD 'replicant'
EOSQL

for((database=1;database<4;database++))
do
psql -v ON_ERROR_STOP=1 --username "postgres" <<-EOSQL
    CREATE DATABASE testdb0${database};
    GRANT ALL PRIVILEGES ON DATABASE testdb0${database} TO replicant;
EOSQL
echo "Created DB testdb0${database}"
for((table=1;table<11;table++))
do
psql -v ON_ERROR_STOP=1 --username "postgres" --dbname "testdb0${database}" <<-EOSQL
    CREATE TABLE d${database}_t${table}
    (
    pole1 serial primary key,
    pole2 varchar(30),
    pole3 varchar(30)     
    );
    INSERT INTO d${database}_t${table} (pole2, pole3)
    VALUES 
    ('test1', 'test1'), 
    ('test1', 'test2'),
    ('test2', 'test1'),
    ('test2', 'test2'),
    ('test3', 'test1');
EOSQL
echo "Created table d${database}_t${table} in DB testdb0${database}"
done
psql -v ON_ERROR_STOP=1 --username "postgres" --dbname "testdb0${database}" <<-EOSQL
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO replicant;
EOSQL
done
psql -v ON_ERROR_STOP=1 --username "postgres" --dbname "testdb01" <<-EOSQL
CREATE PUBLICATION publication1;
    ALTER PUBLICATION publication1 ADD TABLE d1_t1;
    ALTER PUBLICATION publication1 ADD TABLE d1_t3;
    ALTER PUBLICATION publication1 ADD TABLE d1_t5;
    ALTER PUBLICATION publication1 ADD TABLE d1_t7;
    ALTER PUBLICATION publication1 ADD TABLE d1_t9;
EOSQL
psql -v ON_ERROR_STOP=1 --username "postgres" --dbname "testdb03" <<-EOSQL
CREATE PUBLICATION publication2;
    ALTER PUBLICATION publication2 ADD TABLE d3_t2;
    ALTER PUBLICATION publication2 ADD TABLE d3_t4;
    ALTER PUBLICATION publication2 ADD TABLE d3_t6;
    ALTER PUBLICATION publication2 ADD TABLE d3_t8;
    ALTER PUBLICATION publication2 ADD TABLE d3_t10;
EOSQL
