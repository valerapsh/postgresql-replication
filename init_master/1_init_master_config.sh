#!/bin/bash
set -e

echo "wal_level = logical ">> /var/lib/postgresql/data/postgresql.conf
psql -U postgres -c "SELECT pg_reload_conf();"
