#!/bin/bash
set -e

echo "hot_standby = on">> /var/lib/postgresql/data/postgresql.conf
echo "max_replication_slots = 2">> /var/lib/postgresql/data/postgresql.conf
psql -U postgres -c "SELECT pg_reload_conf();"