#!/bin/bash
set -e

echo "Wait 15sec for master initialization"
sleep 15

psql -U postgres -d testdb01 -c "CREATE SUBSCRIPTION subscribe1 CONNECTION 'host=postgres_master port=5432 password=replicant user=replicant dbname=testdb01' PUBLICATION publication1;" 
psql -U postgres -d testdb03 -c "CREATE SUBSCRIPTION subscribe2 CONNECTION 'host=postgres_master port=5432 password=replicant user=replicant dbname=testdb03' PUBLICATION publication2;" 