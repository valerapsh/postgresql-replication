#!/bin/bash
set -e

for database in 1 3
do
psql -v ON_ERROR_STOP=1 --username "postgres" <<-EOSQL
    CREATE DATABASE testdb0${database};
EOSQL
done

for table in 1 3 5 7 9
do
psql -v ON_ERROR_STOP=1 --username "postgres" --dbname "testdb01" <<-EOSQL
    CREATE TABLE d1_t${table}
    (
    pole1 serial primary key,
    pole2 varchar(30),
    pole3 varchar(30)     
    );
EOSQL
done

for table in 2 4 6 8 10
do
psql -v ON_ERROR_STOP=1 --username "postgres" --dbname "testdb03" <<-EOSQL
    CREATE TABLE d3_t${table}
    (
    pole1 serial primary key,
    pole2 varchar(30),
    pole3 varchar(30)     
    );
EOSQL
done
